# Dúvidas relacionadas ao desenvolvimento

## Login
1) Usuario pode fazer login com a digital ou faceId, blz, pegaria nesse caso os dados do usuario e tentaria fazer login na plataforma, mas o servico so disponibiliza login via email/senha. (Não soube como prosseguir a essa opcão, então so disponibilizei um botão para quando for possivel sem fazer nenhuma acão)

## Operations
1) Já que a requisicão de criar operacão não precisa passar o banco, isso seria feito localmente? (Foi passado um param chamado bank, e aparentemente funcionou)

## Transfers

1) Esse servico só seria no dispositivo? (não foi feito dessa forma)
2) Se não, qual endpoint (não tem no arquivo enviado)?

## BankAccounts

1) Precisa de uma tela só pra mostrar o saldo? Se sim, seria o total de todas as contas? (Foi feito dessa forma mesmo, sem mostrar o total em cada banco)
2) Tem um ponto "List", no escopo, mas não diz de que