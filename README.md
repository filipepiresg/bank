# App Finance

## Scope
[Doc](https://www.notion.so/6c54ac767beb4e6d8b5746faa27325cf?v=40559faa407746e69c7cc3ca3251c326)

## Requisitos
- Deve ser utilizado o Redux como gerenciamento de estado
- Pode usar Expo ou CLI
- Persistir o estado no app
- Tratamento de erros
- React Router v5
- Utilizar tabNavigation

## Diferenciais
- Typescript

## Requests
[dropbox](https://www.dropbox.com/s/ngu3x00me913m3i/webholding%20-%20insomnia.json?dl=0)

Url:

[5dev](http://server.5dev.com.br:1337/)