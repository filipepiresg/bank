export default {
  PLACEHOLDER: '#888',
  WHITE: '#fff',
  PRIMARY: '#573FD0',
  SECONDARY: '#F9A64C',
  SHADOW: '#ccc',
  BLACK: '#000',
  ALERT: '#f00',
};
