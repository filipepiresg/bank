import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen');

export default {
  button_opacity: 0.7,
  width,
  height,
};
