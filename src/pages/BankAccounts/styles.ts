import styled from 'styled-components/native';

import { Colors } from '../../styles';

export const Container = styled.View`
  flex: 1;
`;

export const Content = styled.View`
  flex: 1;
  background-color: ${Colors.WHITE};
  align-items: center;
  padding: 10px;
  /* justify-content: center; */
`;

export const PriceText = styled.Text`
  font-size: 29px;
  color: ${Colors.PRIMARY};
  text-align: center;
  font-weight: 600;
`;
export const TitleText = styled.Text`
  font-size: 20px;
  color: ${Colors.BLACK};
  text-align: center;
  font-weight: 300;
`;
