import React, { useMemo } from 'react';
import NumberFormat from 'react-number-format';
import { useSelector } from 'react-redux';

import reduce from 'lodash/reduce';

import { NavBar, BottomTab } from '../../components';
import { BankInterface } from '../../store/modules/ReduxInterfaces';
import { Container, Content, TitleText, PriceText } from './styles';

const BankAccounts: React.FC = () => {
  const banks = useSelector((state: { bank: { banks: BankInterface[] } }) => state.bank.banks);

  const total = useMemo(() => {
    const _total = reduce(
      banks,
      function (sum, bank) {
        const operations = reduce(
          bank.operations,
          function (_sum, operation) {
            if (operation.type === 'outcoming') {
              return _sum - operation.value;
            }
            return _sum + operation.value;
          },
          bank.overdraft
        );
        return sum + operations;
      },
      0
    );

    return _total;
  }, [banks]);

  return (
    <Container>
      <NavBar hasHomeButton />
      <Content>
        <TitleText>Available Balance</TitleText>
        <NumberFormat
          value={total}
          displayType={'text'}
          allowNegative={false}
          fixedDecimalScale
          decimalScale={2}
          thousandSeparator={true}
          prefix={'$ '}
          renderText={(value) => <PriceText>{value}</PriceText>}
        />
      </Content>
      <BottomTab />
    </Container>
  );
};

export default BankAccounts;
