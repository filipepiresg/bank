export { default as BankAccounts } from './BankAccounts';
export { default as Login } from './Login';
export { default as Main } from './Main';
export { default as Operations } from './Operations';
export { default as Registration } from './Registration';
export { default as Transfers } from './Transfers';
