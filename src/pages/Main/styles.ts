import { StyleSheet } from 'react-native';
import IonicIcons from 'react-native-vector-icons/Ionicons';

import styled from 'styled-components/native';

import { Colors, Metrics } from '../../styles';

export const Container = styled.View`
  flex: 1;
`;

export const Content = styled.View`
  flex: 1;
  background-color: ${Colors.WHITE};
`;

export const Separator = styled.View`
  height: 20px;
  width: 20px;
`;

export const CardBank = styled.View`
  width: ${Metrics.width * 0.3}px;
  height: ${Metrics.height * 0.1}px;
  background-color: ${Colors.WHITE};
  align-items: center;
  justify-content: center;
  padding: 10px;
`;

export const CardOperation = styled.View`
  width: 100%;
  height: ${Metrics.height * 0.1}px;
  background-color: ${Colors.WHITE};
  align-items: center;
  justify-content: center;
  flex-direction: row;
  padding: 10px;
`;

export const OperationPrice = styled.Text.attrs({
  numberOfLines: 1,
})`
  font-size: 14px;
  /* color: ${Colors.ALERT}; */
  font-weight: 600;
`;
export const OperationDescription = styled.Text.attrs({
  numberOfLines: 1,
})`
  font-size: 14px;
  color: ${Colors.PRIMARY};
  font-weight: 600;
`;
export const OperationDate = styled.Text`
  font-size: 12px;
  color: ${Colors.PLACEHOLDER};
`;

export const BankName = styled.Text.attrs({
  numberOfLines: 2,
})`
  text-align: center;
  color: ${Colors.BLACK};
  font-size: 15px;
  font-weight: 600;
`;

export const Title = styled.Text`
  font-weight: 600;
  font-size: 18px;
  color: ${Colors.BLACK};
  text-align: left;
  /* margin-bottom: 5px; */
`;

export const ArrowRedo = styled(IonicIcons).attrs({
  name: 'md-arrow-redo-sharp',
  size: 25,
})``;

export default StyleSheet.create({
  list: {
    padding: 10,
  },
  shadow: {
    shadowColor: Colors.SHADOW,
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
  },
  grid: {
    flexDirection: 'column',
  },
  contentListHorizontal: {
    alignItems: 'center',
    justifyContent: 'center',
    // paddingTop: 0,
  },
  iconInverted: {
    transform: [
      {
        rotateX: '180deg',
      },
      {
        rotateY: '180deg',
      },
    ],
  },
  col: {
    // alignItems: 'center',
    justifyContent: 'center',
  },
  paddingContent: {
    marginLeft: 10,
  },
  headerTitle: {
    marginBottom: 10,
  },
  shimmerHorizotal: {
    width: Metrics.width - 20,
    height: Metrics.height * 0.1 + 20,
  },
  shimmer: {
    alignSelf: 'center',
  },
  alignRight: {
    alignItems: 'flex-end',
  },
});
