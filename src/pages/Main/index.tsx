import React, { useCallback, useEffect, useMemo } from 'react';
import { FlatList, View } from 'react-native';
import { Row, Grid, Col } from 'react-native-easy-grid';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import NumberFormat from 'react-number-format';
import { useDispatch, useSelector } from 'react-redux';

import { format, parseISO } from 'date-fns';
import filter from 'lodash/filter';

import { NavBar, BottomTab } from '../../components';
import { BankInterface, OperationInterface } from '../../store/modules/ReduxInterfaces';
import { getAllDataRequest } from '../../store/modules/user/actions';
import Styles, {
  Container,
  Separator,
  CardBank,
  BankName,
  CardOperation,
  OperationPrice,
  OperationDescription,
  OperationDate,
  Title,
} from './styles';

const Main: React.FC = () => {
  const dispatch = useDispatch();

  const { banks, operations, loading } = useSelector(
    (state: {
      operation: { operations: OperationInterface[] };
      bank: { banks: BankInterface[] };
      app: { loading: boolean };
    }) => ({
      operations: state.operation.operations,
      banks: state.bank.banks,
      loading: state.app.loading,
    })
  );

  useEffect(() => {
    dispatch(getAllDataRequest());
  }, []);

  const _banks = useMemo(() => filter(banks, { active: true }), [banks]);

  const renderCardBank = useCallback(({ item }: { item: BankInterface }) => {
    return (
      <CardBank style={Styles.shadow}>
        <BankName>{item.name}</BankName>
      </CardBank>
    );
  }, []);

  const renderCardOperation = useCallback(({ item }: { item: OperationInterface }) => {
    return (
      <CardOperation style={Styles.shadow}>
        <Grid>
          <Col size={1} />
          <Col size={6} style={Styles.col}>
            <OperationDescription>{item.description}</OperationDescription>
            <OperationDate>{format(parseISO(item.created_at), "MMMM',' dd',' yyyy")}</OperationDate>
          </Col>
          <Col size={3} style={[Styles.col, Styles.alignRight]}>
            <NumberFormat
              value={item.value}
              displayType={'text'}
              allowNegative={false}
              fixedDecimalScale
              decimalScale={2}
              thousandSeparator={true}
              prefix={'$'}
              renderText={(value) => {
                const prefix = item.type === 'outcoming' ? '-' : '+';
                return <OperationPrice>{`${prefix} ${value}`}</OperationPrice>;
              }}
            />
          </Col>
        </Grid>
      </CardOperation>
    );
  }, []);

  return (
    <Container>
      <NavBar hasLogoutButton />
      <Grid style={Styles.grid}>
        <View>
          <Title style={Styles.paddingContent}>My banks</Title>
          <ShimmerPlaceHolder
            autoRun
            visible={!loading}
            {...Styles.shimmerHorizotal}
            style={Styles.shimmer}
          >
            <FlatList
              data={_banks}
              horizontal
              renderItem={renderCardBank}
              keyExtractor={(item) => String(item.id)}
              ItemSeparatorComponent={Separator}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={[Styles.list, Styles.contentListHorizontal]}
            />
          </ShimmerPlaceHolder>
        </View>
        <Row size={7}>
          <Col>
            <Title style={Styles.paddingContent}>My Operations</Title>
            <ShimmerPlaceHolder
              autoRun
              visible={!loading}
              {...Styles.shimmerHorizotal}
              style={Styles.shimmer}
            >
              <FlatList
                data={operations}
                renderItem={renderCardOperation}
                keyExtractor={(item) => String(item.id)}
                showsVerticalScrollIndicator={false}
                ItemSeparatorComponent={Separator}
                contentContainerStyle={Styles.list}
              />
            </ShimmerPlaceHolder>
          </Col>
        </Row>
      </Grid>
      <BottomTab />
    </Container>
  );
};

export default Main;
