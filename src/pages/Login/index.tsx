import React, { useCallback, useRef, useEffect, useState } from 'react';
import { TextInput } from 'react-native';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-native';

import * as LocalAuthentication from 'expo-local-authentication';
import { useFormik } from 'formik';

import { Input, Button, StatusBar, ErrorMessage } from '../../components';
import { signInRequest } from '../../store/modules/auth/actions';
import LoginForm from './Schema';
import Styles, { Container, Content, Text } from './styles';

const Login: React.FC = () => {
  const dispatch = useDispatch();

  const [hasHardwareLogin, setHardwareLogin] = useState(false);

  useEffect(() => {
    async function checkHardwareLogin() {
      const result = await LocalAuthentication.hasHardwareAsync();
      const hasEnrolled = await LocalAuthentication.isEnrolledAsync();
      setHardwareLogin(result && hasEnrolled);
    }

    checkHardwareLogin();
  }, []);

  const handleLoginWithFaceFingerprint = useCallback(async () => {
    // const type = await LocalAuthentication.supportedAuthenticationTypesAsync();

    LocalAuthentication.authenticateAsync({
      promptMessage: 'Autenticar com biometria?',
      cancelLabel: 'Cancel',
      disableDeviceFallback: false,
    }).then((_authResult) => {
      // console.tron.log('Authenticate ->', _authResult);
    });
  }, []);

  const usernameRef = useRef<TextInput>(null);
  const passwordRef = useRef<TextInput>(null);

  const handleSubmit = useCallback(
    ({ username, password }) => {
      dispatch(signInRequest({ identifier: username, password }));
    },
    [dispatch]
  );

  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    initialErrors: {
      username: ' ',
      password: ' ',
    },
    validationSchema: LoginForm,
    enableReinitialize: true,
    onSubmit: handleSubmit,
  });

  return (
    <Container>
      <StatusBar />
      <Content>
        <Text>Login</Text>
        <Input
          ref={usernameRef}
          value={formik.values.username}
          placeholder='E-mail'
          onChangeText={(text: string) => {
            formik.setFieldValue('username', text.trim());
          }}
          autoCapitalize='none'
          autoCorrect={false}
          autoCompleteType='email'
          keyboardType='email-address'
          blurOnSubmit={false}
          onSubmitEditing={() => {
            if (passwordRef.current) {
              passwordRef.current.focus();
            }
          }}
          returnKeyType='next'
        />
        <ErrorMessage
          message={formik.touched.username ? formik.errors.username : ''}
          color={Styles.error.color}
        />

        <Input
          ref={passwordRef}
          value={formik.values.password}
          placeholder='Password'
          secureTextEntry
          onChangeText={(text: string) => {
            formik.setFieldValue('password', text);
          }}
          returnKeyType='send'
          autoCapitalize='none'
          autoCorrect={false}
          autoCompleteType='password'
          onSubmitEditing={() => {
            formik.submitForm();
          }}
        />
        <ErrorMessage
          message={formik.touched.password ? formik.errors.password : ''}
          color={Styles.error.color}
        />

        <Button onPress={formik.submitForm} styleButton={{ marginTop: Styles.link.padding }}>
          Sign In
        </Button>
        {hasHardwareLogin && (
          <Text onPress={handleLoginWithFaceFingerprint}>Login with FaceID/Fingerprint</Text>
        )}
      </Content>

      <Link to='/register' underlayColor={Styles.underlay.color} style={Styles.link}>
        <Text>Register</Text>
      </Link>
    </Container>
  );
};

export default Login;
