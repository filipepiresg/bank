import * as Yup from 'yup';

export default Yup.object({
  username: Yup.string().required('E-mail is required').email('E-mail is invalid'),
  password: Yup.string().required('Password is required'),
});
