import { Platform, StyleSheet } from 'react-native';

import { opacify } from 'polished';
import styled from 'styled-components/native';

import { Colors } from '../../styles';

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${Colors.PRIMARY};
`;

export const Content = styled.KeyboardAvoidingView.attrs({
  behavior: Platform.OS === 'ios' ? 'padding' : 'height',
})`
  flex: 1;
  padding: 0 10px;
  align-items: center;
  justify-content: center;
`;

export const Text = styled.Text`
  font-size: 16px;
  font-weight: 500;
  color: ${Colors.WHITE};
  margin-top: 10px;
`;

export default StyleSheet.create({
  link: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    marginBottom: 10,
    alignSelf: 'center',
  },
  underlay: {
    color: opacify(0.2, Colors.PRIMARY),
  },
  error: {
    color: Colors.SECONDARY,
  },
});
