import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import { TextInput } from 'react-native';
import { MaskService } from 'react-native-masked-text';
import Picker from 'react-native-picker-select';
import { useDispatch, useSelector } from 'react-redux';

import { useFormik } from 'formik';
import filter from 'lodash/filter';

import { NavBar, BottomTab, ErrorMessage, Button } from '../../components';
import { getBanksRequest } from '../../store/modules/bank/actions';
import { createOperationRequest } from '../../store/modules/operation/actions';
import { BankInterface } from '../../store/modules/ReduxInterfaces';
import OperationSchema from './Schema';
import Styles, { Container, Content, Input, Title } from './styles';

const Operations: React.FC = () => {
  const dispatch = useDispatch();

  const banks = useSelector((state: { bank: { banks: BankInterface[] } }) => state.bank.banks);

  const valueRef = useRef<TextInput>(null);

  useEffect(() => {
    dispatch(getBanksRequest());
  }, []);

  const handleSubmit = useCallback(
    ({
      description,
      value,
      type,
      bank,
    }: {
      description: string;
      value: string;
      type?: 'incoming' | 'outcming';
      bank?: BankInterface;
    }) => {
      const _value = Number(value.replace(/\D/gi, '')) / 100;

      if (bank !== undefined && _value > 0 && type) {
        dispatch(createOperationRequest({ description, value: _value, bank: bank.id, type }));
      }
    },
    [dispatch]
  );

  const formik = useFormik({
    initialValues: {
      description: '',
      value: '0',
      type: undefined,
      bank: undefined,
    },
    initialErrors: {
      description: ' ',
      value: ' ',
      type: ' ',
      bank: ' ',
    },
    enableReinitialize: true,
    validationSchema: OperationSchema,
    onSubmit: handleSubmit,
  });

  const BANKS = useMemo(
    () =>
      filter(banks, { active: true }).map((bank) => ({
        id: bank.id,
        key: bank.id,
        label: bank.name,
        value: bank,
      })),
    [banks]
  );

  return (
    <Container>
      <NavBar hasHomeButton />
      <Content>
        <Title>Create operation</Title>

        <Picker
          value={formik.values.bank}
          onValueChange={(value) => {
            formik.setFieldValue('bank', value);
          }}
          items={BANKS}
          placeholder={{ label: 'Select the bank...', value: null, key: null }}
          doneText='Done'
          useNativeAndroidPickerStyle={false}
          style={Styles}
        />
        <ErrorMessage message={formik.touched.bank ? formik.errors.bank : ' '} />
        <Input
          value={formik.values.description}
          onChangeText={(text) => {
            formik.setFieldValue('description', text);
          }}
          placeholder='Insert description here...'
          autoCapitalize='sentences'
          autoCorrect={false}
          keyboardType='default'
          returnKeyType='next'
          onSubmitEditing={() => {
            if (valueRef.current) valueRef.current.focus();
          }}
        />
        <ErrorMessage message={formik.touched.description ? formik.errors.description : ' '} />
        <Picker
          value={formik.values.type}
          onValueChange={(value) => {
            formik.setFieldValue('type', value);
          }}
          items={[
            { key: 1, value: 'incoming', label: 'Incoming' },
            { key: 2, value: 'outcoming', label: 'Outcoming' },
          ]}
          placeholder={{ label: 'Select the operation type...', value: null, key: null }}
          doneText='Done'
          useNativeAndroidPickerStyle={false}
          style={Styles}
        />
        <ErrorMessage message={formik.touched.type ? formik.errors.type : ' '} />
        <Input
          value={MaskService.toMask('money', String(formik.values.value), {
            precision: 2,
            separator: '.',
            delimiter: ',',
            unit: '$ ',
            suffixUnit: '',
          })}
          onChangeText={(value) => {
            formik.setFieldValue('value', value);
          }}
          ref={valueRef}
          autoCapitalize='none'
          autoCorrect={false}
          keyboardType='numbers-and-punctuation'
          returnKeyType='send'
          onSubmitEditing={formik.submitForm}
        />
        <ErrorMessage message={formik.touched.value ? formik.errors.value : ' '} />
        <Button styleButton={Styles.button} onPress={formik.submitForm}>
          Send
        </Button>
      </Content>
      <BottomTab />
    </Container>
  );
};

export default Operations;
