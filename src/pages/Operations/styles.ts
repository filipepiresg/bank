import { StyleSheet } from 'react-native';

import styled from 'styled-components/native';

import { Colors, Metrics } from '../../styles';

export const Container = styled.View`
  flex: 1;
`;

export const Content = styled.View`
  flex: 1;
  background-color: ${Colors.WHITE};
  padding: 10px;
`;

export const Title = styled.Text`
  font-size: 20px;
  color: ${Colors.PRIMARY};
  text-align: center;
  margin-bottom: 5px;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: Colors.SHADOW,
})`
  align-self: center;
  background-color: ${Colors.WHITE};
  font-size: 14px;
  padding: 10px;
  border: 1px solid ${Colors.SHADOW};
  border-radius: 5px;
  color: ${Colors.BLACK};
  width: 100%;
`;

export default StyleSheet.create({
  button: { width: Metrics.width * 0.85, alignSelf: 'center' },
  inputAndroid: {
    alignSelf: 'center',
    backgroundColor: Colors.WHITE,
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: Colors.SHADOW,
    borderRadius: 5,
    color: Colors.BLACK,
    width: '100%',
  },
  inputIOS: {
    alignSelf: 'center',
    backgroundColor: Colors.WHITE,
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: Colors.SHADOW,
    borderRadius: 5,
    color: Colors.BLACK,
    width: '100%',
  },
  iconContainer: {
    top: 8,
    right: 0,
  },
  placeholder: {
    color: Colors.SHADOW,
  },
});
