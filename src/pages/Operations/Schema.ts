import * as yup from 'yup';

export default yup.object({
  description: yup.string().required('Description is required').trim('Description is invalid'),
  value: yup.string().required('Value is required').trim('Value is invalid'),
  type: yup.string().required('Operation type is required'),
  bank: yup.string().required('Bank is required').nullable(),
});
