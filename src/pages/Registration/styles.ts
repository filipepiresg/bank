import { Platform } from 'react-native';

import styled from 'styled-components/native';

import { Colors } from '../../styles';

export const Container = styled.SafeAreaView`
  flex: 1;
`;

export const Content = styled.KeyboardAvoidingView.attrs({
  behavior: Platform.OS === 'ios' ? 'padding' : 'height',
})`
  flex: 1;
  margin: 10px;
  align-items: center;
`;

export const Text = styled.Text`
  font-size: 16px;
  font-weight: 500;
  color: ${Colors.BLACK};
`;
