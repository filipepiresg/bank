import * as yup from 'yup';

export default yup.object({
  username: yup.string().required('Identifier is required'),
  email: yup.string().email('E-mail is invalid').required('E-mail is required'),
  password: yup.string().required('Password is required'),
});
