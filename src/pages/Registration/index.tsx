import React, { useCallback, useRef } from 'react';
import { TextInput } from 'react-native';
import { useDispatch } from 'react-redux';

import { useFormik } from 'formik';

import { NavBar, Input, Button, ErrorMessage } from '../../components';
import { signUpRequest } from '../../store/modules/auth/actions';
import RegisterForm from './Schema';
import { Container, Content, Text } from './styles';

const Registration: React.FC = () => {
  const dispatch = useDispatch();

  const usernameRef = useRef<TextInput>(null);
  const emailRef = useRef<TextInput>(null);
  const passwordRef = useRef<TextInput>(null);

  const handleSubmit = useCallback(
    ({ email, username, password }) => {
      dispatch(
        signUpRequest({
          email,
          username,
          password,
        })
      );
    },
    [dispatch]
  );

  const formik = useFormik({
    initialValues: {
      username: '',
      email: '',
      password: '',
    },
    initialErrors: {
      username: ' ',
      email: ' ',
      password: ' ',
    },
    validationSchema: RegisterForm,
    onSubmit: handleSubmit,
    enableReinitialize: true,
  });
  return (
    <>
      <NavBar hasArrowback />
      <Container>
        <Content>
          <Text>Register here</Text>
          <Input
            ref={usernameRef}
            value={formik.values.username}
            placeholder='Username'
            onChangeText={(txt: string) => {
              formik.setFieldValue('username', txt);
            }}
            keyboardType='numbers-and-punctuation'
            autoCapitalize='none'
            autoCompleteType='username'
            autoCorrect={false}
            returnKeyType='next'
            blurOnSubmit={false}
            onSubmitEditing={() => {
              if (emailRef.current) {
                emailRef.current.focus();
              }
            }}
          />
          <ErrorMessage message={formik.touched.username ? formik.errors.username : ''} />

          <Input
            ref={emailRef}
            value={formik.values.email}
            placeholder='E-mail'
            onChangeText={(txt: string) => {
              formik.setFieldValue('email', txt.trim());
            }}
            keyboardType='email-address'
            autoCapitalize='none'
            autoCompleteType='email'
            returnKeyType='next'
            blurOnSubmit={false}
            onSubmitEditing={() => {
              if (passwordRef.current) {
                passwordRef.current.focus();
              }
            }}
          />
          <ErrorMessage message={formik.touched.email ? formik.errors.email : ''} />

          <Input
            ref={passwordRef}
            value={formik.values.password}
            placeholder='Password'
            onChangeText={(txt: string) => {
              formik.setFieldValue('password', txt);
            }}
            keyboardType='numbers-and-punctuation'
            autoCapitalize='none'
            autoCompleteType='password'
            autoCorrect={false}
            returnKeyType='send'
            onSubmitEditing={() => {
              formik.submitForm();
            }}
          />
          <ErrorMessage message={formik.touched.password ? formik.errors.password : ''} />

          <Button onPress={formik.submitForm} styleButton={{ marginTop: 10 }}>
            Register
          </Button>
        </Content>
      </Container>
    </>
  );
};

export default Registration;
