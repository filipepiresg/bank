import React, { useCallback, useEffect, useMemo } from 'react';
import { MaskService } from 'react-native-masked-text';
import Picker from 'react-native-picker-select';
import { useDispatch, useSelector } from 'react-redux';

import { useFormik } from 'formik';
import differenceBy from 'lodash/differenceBy';
import filter from 'lodash/filter';
import get from 'lodash/get';

import { Button, NavBar, BottomTab, ErrorMessage } from '../../components';
import { getBanksRequest } from '../../store/modules/bank/actions';
import { transferRequest } from '../../store/modules/operation/actions';
import { BankInterface } from '../../store/modules/ReduxInterfaces';
import TransferForm from './Schema';
import Styles, { Container, Content, Title, Input } from './styles';

const Transfers: React.FC = () => {
  const dispatch = useDispatch();

  const banks = useSelector((state: { bank: { banks: BankInterface[] } }) => state.bank.banks);

  useEffect(() => {
    dispatch(getBanksRequest());
  }, []);

  const handleSubmit = useCallback(
    ({
      bankOutcoming,
      bankIncoming,
      value,
    }: {
      bankOutcoming?: { id: number };
      bankIncoming?: { id: number };
      value: string;
    }) => {
      const _value = Number(value.replace(/\D/gi, '')) / 100;
      if (bankOutcoming !== undefined && bankIncoming !== undefined && _value > 0) {
        dispatch(
          transferRequest({
            bankOutcoming: bankOutcoming.id,
            bankIncoming: bankIncoming.id,
            value: _value,
          })
        );
      }
    },
    [dispatch]
  );

  const formik = useFormik({
    initialValues: {
      bankOutcoming: undefined,
      bankIncoming: undefined,
      value: '0',
    },
    initialErrors: {
      bankOutcoming: ' ',
      bankIncoming: ' ',
      value: ' ',
    },
    enableReinitialize: true,
    validationSchema: TransferForm,
    onSubmit: handleSubmit,
  });

  const BANKS = useMemo(
    () =>
      filter(banks, { active: true }).map((bank) => ({
        id: bank.id,
        key: bank.id,
        label: bank.name,
        value: bank,
      })),
    [banks]
  );

  const BANKS_DIFERENCES = useMemo(() => {
    const differences = differenceBy(BANKS, [formik.values.bankOutcoming], 'id');
    return get(formik, 'values.bankOutcoming', null) ? differences : [];
  }, [BANKS, formik]);

  return (
    <Container>
      <NavBar hasHomeButton />
      <Content>
        <Title>Transfers between your accounts</Title>
        <Picker
          value={formik.values.bankOutcoming}
          onValueChange={(value) => {
            formik.setFieldValue('bankOutcoming', value);
          }}
          items={BANKS}
          placeholder={{ label: 'Select the bank to send...', value: null, key: null }}
          doneText='Done'
          useNativeAndroidPickerStyle={false}
          style={Styles}
        />
        <ErrorMessage message={formik.touched.bankOutcoming ? formik.errors.bankOutcoming : ' '} />
        <Picker
          value={formik.values.bankIncoming}
          onValueChange={(value) => {
            formik.setFieldValue('bankIncoming', value);
          }}
          placeholder={{ label: 'Select the bank to receive...', value: null, key: null }}
          items={BANKS_DIFERENCES}
          doneText='Done'
          useNativeAndroidPickerStyle={false}
          style={Styles}
        />
        <ErrorMessage message={formik.touched.bankIncoming ? formik.errors.bankIncoming : ' '} />
        <Input
          value={MaskService.toMask('money', String(formik.values.value), {
            precision: 2,
            separator: '.',
            delimiter: ',',
            unit: '$ ',
            suffixUnit: '',
          })}
          onChangeText={(value) => {
            formik.setFieldValue('value', value);
          }}
          autoCapitalize='none'
          autoCorrect={false}
          keyboardType='numbers-and-punctuation'
          returnKeyType='send'
          onSubmitEditing={formik.submitForm}
        />
        <ErrorMessage message={formik.touched.value ? formik.errors.value : ' '} />
        <Button styleButton={Styles.button} onPress={formik.submitForm}>
          Transfer
        </Button>
      </Content>
      <BottomTab />
    </Container>
  );
};

export default Transfers;
