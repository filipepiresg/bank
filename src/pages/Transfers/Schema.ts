import * as yup from 'yup';

export default yup.object({
  value: yup.string().required('Transfer amount is required').trim('Amount is invalid'),
  bankOutcoming: yup.string().required('Bank outcoming is required').nullable(),
  bankIncoming: yup.string().required('Bank incoming is required').nullable(),
});
