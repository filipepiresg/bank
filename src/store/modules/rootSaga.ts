import { all } from 'redux-saga/effects';

import auth from './auth/sagas';
import bank from './bank/sagas';
import operation from './operation/sagas';
import user from './user/sagas';

function* rootSaga() {
  return yield all([auth, bank, operation, user]);
}

export default rootSaga;
