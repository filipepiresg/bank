import { combineReducers } from 'redux';

import app from './app/reducer';
import auth from './auth/reducer';
import bank from './bank/reducer';
import operation from './operation/reducer';
import user from './user/reducer';

export default combineReducers({
  auth,
  app,
  bank,
  operation,
  user,
});
