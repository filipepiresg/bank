import { ActionInterface } from '../ReduxInterfaces';
import * as AuthTypes from './types';

export interface ISignInRequestPayload {
  identifier: string;
  password: string;
}

export interface ISignInSuccessPayload {
  jwt: string;
  user: any;
}

export interface ISignUpRequestPayload {
  username: string;
  password: string;
  email: string;
}

export function signInRequest(payload: ISignInRequestPayload): ActionInterface {
  return {
    type: AuthTypes.SIGNIN_REQUEST,
    payload,
  };
}

export function signInSuccess(payload: ISignInSuccessPayload): ActionInterface {
  return {
    type: AuthTypes.SIGNIN_SUCCESS,
    payload,
  };
}

export function signInFailure(): ActionInterface {
  return {
    type: AuthTypes.SIGNIN_FAILURE,
  };
}

export function signUpRequest(payload: ISignUpRequestPayload): ActionInterface {
  return {
    type: AuthTypes.SIGNUP_REQUEST,
    payload,
  };
}

export function signUpSuccess(payload: ISignInSuccessPayload): ActionInterface {
  return {
    type: AuthTypes.SIGNUP_SUCCESS,
    payload,
  };
}

export function signUpFailure(): ActionInterface {
  return {
    type: AuthTypes.SIGNUP_FAILURE,
  };
}

export function logoutRequest(): ActionInterface {
  return {
    type: AuthTypes.LOGOUT_REQUEST,
  };
}

export function logout(): ActionInterface {
  return {
    type: AuthTypes.LOGOUT,
  };
}
