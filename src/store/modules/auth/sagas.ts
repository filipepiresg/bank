import { Alert } from 'react-native';
import SplashScreen from 'react-native-splash-screen';

import get from 'lodash/get';
import { takeLatest, call, put, all } from 'redux-saga/effects';

import api, { Endpoints } from '../../../services/api';
import {
  signInFailure,
  signInSuccess,
  signUpFailure,
  signUpSuccess,
  logout,
  ISignInRequestPayload,
  ISignUpRequestPayload,
} from './actions';
import * as AuthTypes from './types';

export function* signIn({ payload }: { payload: ISignInRequestPayload }): void {
  try {
    const response = yield call(api.post, Endpoints.SIGNIN, payload);
    const jwt = get(response, 'data.jwt');

    if (jwt) {
      api.defaults.headers.Authorization = `Bearer ${jwt}`;
    }
    yield put(signInSuccess(response.data));
  } catch (error) {
    let title = 'Something unexpected happened';
    let message = 'Please try again in a moment';

    const errors = get(error, 'response.data.message[0].messages[0].message', null);
    const data = get(error, 'response.data.data[0].messages[0].message', null);

    if (errors) {
      if (typeof errors === 'string') {
        title = 'Check your credentials';
        message = errors;
      }
    } else if (data) {
      if (typeof data === 'string') {
        title = 'Check your credentials';
        message = errors;
      }
    }

    Alert.alert(title, message);
    yield put(signInFailure());
  }
}
export function* signUp({ payload }: { payload: ISignUpRequestPayload }): void {
  try {
    const response = yield call(api.post, Endpoints.SIGNUP, payload);
    const jwt = get(response, 'data.jwt');

    if (jwt) {
      api.defaults.headers.Authorization = `Bearer ${jwt}`;
    }
    yield put(signUpSuccess(response.data));
  } catch (error) {
    let title = 'Something unexpected happened';
    let message = 'Please try again in a moment';

    const errors = get(error, 'response.data.message[0].messages[0].message', null);
    const data = get(error, 'response.data.data[0].messages[0].message', null);

    if (errors) {
      if (typeof errors === 'string') {
        title = 'Check your data';
        message = errors;
      }
    } else if (data) {
      if (typeof data === 'string') {
        title = 'Check your data';
        message = errors;
      }
    }

    Alert.alert(title, message);
    yield put(signUpFailure());
  }
}
export function* signOut(): void {
  if (get(api, 'defaults.headers.Authorization')) delete api.defaults.headers.Authorization;

  yield put(logout());
}

export function setToken(action: { payload?: { auth: { jwt: string; signed: boolean } } }): void {
  if (action.payload) {
    const { payload } = action;

    if (get(payload, 'auth.signed', null)) {
      api.defaults.headers.Authorization = `Bearer ${get(payload, 'auth.jwt', '')}`;
    }
  }

  SplashScreen.hide();
}

export default all([
  takeLatest('persist/REHYDRATE', setToken),
  takeLatest(AuthTypes.SIGNUP_REQUEST, signUp),
  takeLatest(AuthTypes.SIGNIN_REQUEST, signIn),
  takeLatest(AuthTypes.LOGOUT_REQUEST, signOut),
]);
