import produce from 'immer';

import { ActionInterface } from '../ReduxInterfaces';
import * as AuthTypes from './types';

export interface AuthStateInterface {
  signed: boolean;
  jwt: string | null;
}

const INITIAL_STATE: AuthStateInterface = {
  signed: false,
  jwt: null,
};

function auth(state = INITIAL_STATE, action: ActionInterface): AuthStateInterface {
  return produce(state, (draft) => {
    switch (action.type) {
      case AuthTypes.LOGOUT: {
        draft.signed = false;
        draft.jwt = null;
        break;
      }
      case AuthTypes.SIGNIN_SUCCESS: {
        draft.jwt = action.payload.jwt;
        draft.signed = true;
        break;
      }
      // case AuthTypes.SIGNUP_SUCCESS: {
      //   draft.jwt = action.payload.jwt;
      //   draft.signed = true;
      //   break;
      // }
      default:
    }
  });
}

export default auth;
