export const SIGNIN_REQUEST = '@auth/SIGNIN_REQUEST';
export const SIGNIN_SUCCESS = '@auth/SIGNIN_SUCCESS';
export const SIGNIN_FAILURE = '@auth/SIGNIN_FAILURE';

export const SIGNUP_REQUEST = '@auth/SIGNUP_REQUEST';
export const SIGNUP_SUCCESS = '@auth/SIGNUP_SUCCESS';
export const SIGNUP_FAILURE = '@auth/SIGNUP_FAILURE';

export const LOGOUT_REQUEST = '@auth/LOGOUT_REQUEST';
export const LOGOUT = '@auth/LOGOUT';
