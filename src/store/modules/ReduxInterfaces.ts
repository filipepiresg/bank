export interface ActionInterface {
  type: string;
  payload?: any;
}

export interface BankInterface {
  id: number;
  name: string;
  active: boolean;
  overdraft: number;
  user: {
    id: number;
    username: string;
    email: string;
    provider: string;
    confirmed: boolean;
    blocked: boolean | null;
    role: number;
    created_at: string;
    updated_at: string;
  };
  teste: string | null;
  created_at: string;
  updated_at: string;
  file: string | null;
  operations: [
    {
      id: number;
      description: string;
      value: number;
      type: string;
      user: number;
      created_at: string;
      updated_at: string;
      file: string | null;
    }
  ];
}

export interface OperationInterface {
  id: number;
  description: string;
  value: number;
  type: string;
  user: {
    id: number;
    username: string;
    email: string;
    provider: string;
    confirmed: boolean;
    blocked: boolean | null;
    role: number;
    created_at: string;
    updated_at: string;
  };
  created_at: string;
  updated_at: string;
  file: string | null;
}
