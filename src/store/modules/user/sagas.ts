import { all, call, takeLatest, put } from 'redux-saga/effects';

import api, { Endpoints } from '../../../services/api';
import { getAllDataFailures, getAllDataSuccess } from './actions';
import * as UserTypes from './types';

function* getAllData(): void {
  try {
    const responseBanks = yield call(api.get, Endpoints.BANK_ACCOUNT);
    const responseOperations = yield call(api.get, Endpoints.OPERATIONS);

    yield put(
      getAllDataSuccess({ banks: responseBanks.data, operations: responseOperations.data })
    );
  } catch (error) {
    if (__DEV__) {
      console.log('ERROR ON GET ALL DATA', error);
    }
    yield put(getAllDataFailures());
  }
}

export default all([takeLatest(UserTypes.GET_ALL_DATA_REQUEST, getAllData)]);
