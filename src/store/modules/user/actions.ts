import { ActionInterface, OperationInterface, BankInterface } from '../ReduxInterfaces';
import * as UserTypes from './types';

interface IGetAllDataPayload {
  banks: BankInterface[];
  operations: OperationInterface[];
}

export function getAllDataRequest(): ActionInterface {
  return {
    type: UserTypes.GET_ALL_DATA_REQUEST,
  };
}

export function getAllDataSuccess(payload: IGetAllDataPayload): ActionInterface {
  return {
    type: UserTypes.GET_ALL_DATA_SUCCESS,
    payload,
  };
}
export function getAllDataFailures(): ActionInterface {
  return {
    type: UserTypes.GET_ALL_DATA_FAILURE,
  };
}
