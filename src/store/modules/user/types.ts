export const GET_ALL_DATA_REQUEST = '@user/GET_ALL_DATA_REQUEST';
export const GET_ALL_DATA_SUCCESS = '@user/GET_ALL_DATA_SUCCESS';
export const GET_ALL_DATA_FAILURE = '@user/GET_ALL_DATA_FAILURE';
