import produce from 'immer';

import * as AuthTypes from '../auth/types';
import { ActionInterface } from '../ReduxInterfaces';

export interface UserInterface {
  profile: {
    id: number;
    username: string;
    email: string;
    provider: string;
    confirmed: boolean;
    blocked: boolean | null;
    role: {
      id: number;
      name: string;
      description: string;
      type: string;
    };
    created_at: string;
    updated_at: string;
  } | null;
}

const INITIAL_STATE: UserInterface = {
  profile: null,
};

function user(state = INITIAL_STATE, action: ActionInterface): UserInterface {
  return produce(state, (draft) => {
    switch (action.type) {
      case AuthTypes.LOGOUT: {
        draft.profile = null;
        break;
      }
      case AuthTypes.SIGNIN_SUCCESS: {
        draft.profile = action.payload.user;
        break;
      }
      case AuthTypes.SIGNUP_SUCCESS: {
        draft.profile = action.payload.user;
        break;
      }
      default:
    }
  });
}

export default user;
