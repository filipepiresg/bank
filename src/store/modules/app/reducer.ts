import produce from 'immer';

import * as AuthTypes from '../auth/types';
import * as BankTypes from '../bank/types';
import * as OperationTypes from '../operation/types';
import { ActionInterface } from '../ReduxInterfaces';
import * as UserTypes from '../user/types';
import * as AppTypes from './types';

export interface AppInterface {
  loading: boolean;
}

const INITIAL_STATE = {
  loading: false,
};

function app(state = INITIAL_STATE, action: ActionInterface): AppInterface {
  return produce(state, (draft) => {
    switch (action.type) {
      case AuthTypes.SIGNIN_REQUEST:
      case AuthTypes.SIGNUP_REQUEST:
      case BankTypes.GET_BANKS_REQUEST:
      case BankTypes.CREATE_BANK_REQUEST:
      case OperationTypes.GET_OPERATION_REQUEST:
      case OperationTypes.CREATE_OPERATION_REQUEST:
      case OperationTypes.TRANSFER_REQUEST:
      case UserTypes.GET_ALL_DATA_REQUEST:
      case AppTypes.START_REQUEST: {
        draft.loading = true;
        break;
      }
      case AuthTypes.SIGNIN_FAILURE:
      case AuthTypes.SIGNUP_FAILURE:
      case AuthTypes.SIGNIN_SUCCESS:
      case AuthTypes.SIGNUP_SUCCESS:
      case BankTypes.GET_BANKS_FAILURE:
      case BankTypes.GET_BANKS_SUCCESS:
      case BankTypes.CREATE_BANK_FAILURE:
      case BankTypes.CREATE_BANK_SUCCESS:
      case OperationTypes.GET_OPERATION_SUCCESS:
      case OperationTypes.GET_OPERATION_FAILURE:
      case OperationTypes.CREATE_OPERATION_SUCCESS:
      case OperationTypes.CREATE_OPERATION_FAILURE:
      case OperationTypes.TRANSFER_SUCCESS:
      case OperationTypes.TRANSFER_FAILURE:
      case UserTypes.GET_ALL_DATA_SUCCESS:
      case UserTypes.GET_ALL_DATA_FAILURE:
      case AppTypes.STOP_REQUEST: {
        draft.loading = false;
        break;
      }
      default:
    }
  });
}

export default app;
