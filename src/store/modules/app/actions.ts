import { ActionInterface } from '../ReduxInterfaces';
import * as AppTypes from './types';

export function startRequest(): ActionInterface {
  return {
    type: AppTypes.START_REQUEST,
  };
}

export function stopRequest(): ActionInterface {
  return {
    type: AppTypes.STOP_REQUEST,
  };
}
