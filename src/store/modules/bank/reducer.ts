import produce from 'immer';

import * as AuthTypes from '../auth/types';
import { ActionInterface, BankInterface } from '../ReduxInterfaces';
import * as UserTypes from '../user/types';
import * as BankTypes from './types';

export interface BankStateInterface {
  banks: BankInterface[];
}

const INITIAL_STATE: BankStateInterface = {
  banks: [],
};

function bank(state = INITIAL_STATE, action: ActionInterface): BankStateInterface {
  return produce(state, (draft) => {
    switch (action.type) {
      case UserTypes.GET_ALL_DATA_SUCCESS: {
        draft.banks = action.payload.banks;
        break;
      }
      case BankTypes.GET_BANKS_SUCCESS: {
        draft.banks = action.payload;
        break;
      }
      case BankTypes.CREATE_BANK_SUCCESS: {
        draft.banks = [...draft.banks, action.payload];
        break;
      }
      case AuthTypes.LOGOUT: {
        draft.banks = [];
        break;
      }
      default:
    }
  });
}

export default bank;
