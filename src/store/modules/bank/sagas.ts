import { takeLatest, call, put, all } from 'redux-saga/effects';

import api, { Endpoints } from '../../../services/api';
import {
  getBanksFailure,
  createBankFailure,
  getBanksSuccess,
  createBankSuccess,
  ICreateBankPayload,
} from './actions';
import * as BankTypes from './types';

export function* getBanks(): void {
  try {
    const response = yield call(api.get, Endpoints.BANK_ACCOUNT);

    yield put(getBanksSuccess(response.data));
  } catch (error) {
    yield put(getBanksFailure());
  }
}
export function* createBank({ payload }: { payload: ICreateBankPayload }): void {
  try {
    const response = yield call(api.post, Endpoints.BANK_ACCOUNT, { payload });

    yield put(createBankSuccess(response.data));
  } catch (error) {
    yield put(createBankFailure());
  }
}
export default all([
  takeLatest(BankTypes.CREATE_BANK_REQUEST, createBank),
  takeLatest(BankTypes.GET_BANKS_REQUEST, getBanks),
]);
