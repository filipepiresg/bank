import { ActionInterface, BankInterface } from '../ReduxInterfaces';
import * as BankTypes from './types';

export interface ICreateBankPayload {
  name: string;
  overcraft: number;
}

export function getBanksRequest(): ActionInterface {
  return {
    type: BankTypes.GET_BANKS_REQUEST,
  };
}

export function getBanksSuccess(payload: BankInterface[]): ActionInterface {
  return {
    type: BankTypes.GET_BANKS_SUCCESS,
    payload,
  };
}
export function getBanksFailure(): ActionInterface {
  return {
    type: BankTypes.GET_BANKS_FAILURE,
  };
}

export function createBankRequest(payload: ICreateBankPayload): ActionInterface {
  return {
    type: BankTypes.CREATE_BANK_REQUEST,
    payload,
  };
}

export function createBankFailure(): ActionInterface {
  return {
    type: BankTypes.CREATE_BANK_FAILURE,
  };
}

export function createBankSuccess(payload: BankInterface): ActionInterface {
  return {
    type: BankTypes.CREATE_BANK_SUCCESS,
    payload,
  };
}
