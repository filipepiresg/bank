import produce from 'immer';

import * as AuthTypes from '../auth/types';
import { ActionInterface, OperationInterface } from '../ReduxInterfaces';
import * as UserTypes from '../user/types';
import * as OperationTypes from './types';

export interface OperationStateInterface {
  operations: OperationInterface[];
}

const INITIAL_STATE: OperationStateInterface = {
  operations: [],
};

function operation(state = INITIAL_STATE, action: ActionInterface): OperationStateInterface {
  return produce(state, (draft) => {
    switch (action.type) {
      case UserTypes.GET_ALL_DATA_SUCCESS: {
        draft.operations = action.payload.operations;
        break;
      }
      case OperationTypes.TRANSFER_SUCCESS:
      case OperationTypes.GET_OPERATION_SUCCESS: {
        draft.operations = action.payload;
        break;
      }
      case OperationTypes.CREATE_OPERATION_SUCCESS: {
        draft.operations = [...draft.operations, action.payload];
        break;
      }
      case AuthTypes.LOGOUT: {
        draft.operations = [];
        break;
      }
      default:
    }
  });
}

export default operation;
