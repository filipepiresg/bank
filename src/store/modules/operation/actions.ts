import { ActionInterface, OperationInterface } from '../ReduxInterfaces';
import * as OperationTypes from './types';

export interface ICreateOperationPayload {
  description: string;
  value: number;
  bank: number;
  type: string;
}

export interface ITransferPayload {
  bankOutcoming: number;
  bankIncoming: number;
  value: number;
}

export function getOperationRequest(): ActionInterface {
  return {
    type: OperationTypes.GET_OPERATION_REQUEST,
  };
}

export function getOperationSuccess(payload: OperationInterface[]): ActionInterface {
  return {
    type: OperationTypes.GET_OPERATION_SUCCESS,
    payload,
  };
}

export function getOperationFailure(): ActionInterface {
  return {
    type: OperationTypes.GET_OPERATION_FAILURE,
  };
}

export function createOperationRequest(payload: ICreateOperationPayload): ActionInterface {
  return {
    type: OperationTypes.CREATE_OPERATION_REQUEST,
    payload,
  };
}

export function createOperationSuccess(payload: OperationInterface): ActionInterface {
  return {
    type: OperationTypes.CREATE_OPERATION_SUCCESS,
    payload,
  };
}

export function createOperationFailure(): ActionInterface {
  return {
    type: OperationTypes.CREATE_OPERATION_FAILURE,
  };
}

export function transferRequest(payload: ITransferPayload): ActionInterface {
  return {
    type: OperationTypes.TRANSFER_REQUEST,
    payload,
  };
}

export function transferSuccess(payload: OperationInterface[]): ActionInterface {
  return {
    type: OperationTypes.TRANSFER_SUCCESS,
    payload,
  };
}

export function transferFailure(): ActionInterface {
  return {
    type: OperationTypes.TRANSFER_FAILURE,
  };
}
