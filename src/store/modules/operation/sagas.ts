import { takeLatest, call, put, all } from 'redux-saga/effects';

import api, { Endpoints } from '../../../services/api';
import {
  getOperationFailure,
  createOperationFailure,
  getOperationSuccess,
  createOperationSuccess,
  ICreateOperationPayload,
  ITransferPayload,
  transferSuccess,
  transferFailure,
} from './actions';
import * as OperationTypes from './types';

export function* getOperations(): void {
  try {
    const response = yield call(api.get, Endpoints.OPERATIONS);

    yield put(getOperationSuccess(response.data));
  } catch (error) {
    yield put(getOperationFailure());
  }
}

export function* createOperation({ payload }: { payload: ICreateOperationPayload }): void {
  try {
    const response = yield call(api.post, Endpoints.OPERATIONS, payload);

    yield put(createOperationSuccess(response.data));
  } catch (error) {
    yield put(createOperationFailure());
  }
}

// FIXME verificar como é feito a transferencia
export function* handleTransfer({ payload }: { payload: ITransferPayload }): void {
  try {
    yield call(api.post, Endpoints.OPERATIONS, {
      bank: payload.bankOutcoming,
      type: 'outcoming',
      description: 'Transfer',
      value: payload.value,
    });
    yield call(api.post, Endpoints.OPERATIONS, {
      bank: payload.bankIncoming,
      type: 'incoming',
      description: 'Transfer',
      value: payload.value,
    });

    const response = yield call(api.get, Endpoints.OPERATIONS);

    yield put(transferSuccess(response.data));
  } catch (error) {
    yield put(transferFailure());
  }
}

export default all([
  takeLatest(OperationTypes.CREATE_OPERATION_REQUEST, createOperation),
  takeLatest(OperationTypes.GET_OPERATION_REQUEST, getOperations),
  takeLatest(OperationTypes.TRANSFER_REQUEST, handleTransfer),
]);
