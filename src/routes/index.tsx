import React from 'react';
import { useSelector } from 'react-redux';
import { NativeRouter, Route, Redirect, Switch } from 'react-router-native';

import get from 'lodash/get';

import { Login, Registration, Main, BankAccounts, Operations, Transfers } from '../pages';

const MainRoutes: React.FC = () => {
  const isSigned: boolean = useSelector((state: { auth: { signed: boolean } }) =>
    get(state, 'auth.signed', false)
  );

  return (
    <NativeRouter>
      <Switch>
        <Route path='/' exact>
          {!isSigned ? <Login /> : <Main />}
        </Route>
        <Route path='/register' component={Registration} />
        <Route path='/banks'>{!isSigned ? <Redirect to='/' /> : <BankAccounts />}</Route>
        <Route path='/operations'>{!isSigned ? <Redirect to='/' /> : <Operations />}</Route>
        <Route path='/transfers'>{!isSigned ? <Redirect to='/' /> : <Transfers />}</Route>
        <Route>{!isSigned ? <Login /> : <Main />}</Route>
      </Switch>
    </NativeRouter>
  );
};

export default MainRoutes;
