import styled from 'styled-components/native';

import { Colors } from '../styles';

export default styled.StatusBar.attrs({
  animated: true,
  backgroundColor: Colors.PRIMARY,
  barStyle: 'light-content',
})``;
