import React, { memo, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-native';

import { logoutRequest } from '../../store/modules/auth/actions';
import StatusBar from '../StatusBar';
import {
  Container,
  Button,
  Content,
  ArrowBack,
  EmptyContainer,
  HomeIcon,
  ExitIcon,
} from './styles';

interface Props {
  hasArrowback?: boolean;
  hasHomeButton?: boolean;
  hasLogoutButton?: boolean;
  title?: string;
}

const NavBar: React.FC<Props> = ({ hasArrowback, hasHomeButton, hasLogoutButton }) => {
  const dispatch = useDispatch();

  const handleLogout = useCallback(() => {
    dispatch(logoutRequest());
  }, [dispatch]);

  return (
    <Container>
      <StatusBar />
      <Content>
        {hasArrowback && (
          <Link to='/'>
            <ArrowBack />
          </Link>
        )}
        {!hasArrowback && hasHomeButton && (
          <Link to='/'>
            <HomeIcon />
          </Link>
        )}
        {!(hasArrowback || hasHomeButton) && <EmptyContainer />}
        {/* <Text>{title}</Text> */}
        {hasLogoutButton && (
          <Button onPress={handleLogout}>
            <ExitIcon />
          </Button>
        )}
      </Content>
    </Container>
  );
};

NavBar.defaultProps = {
  hasArrowback: false,
  hasHomeButton: false,
  hasLogoutButton: false,
  title: '',
};

export default memo(NavBar);
