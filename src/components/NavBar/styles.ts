import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import styled from 'styled-components/native';

import { Colors, Metrics } from '../../styles';

export const Container = styled.SafeAreaView`
  background-color: ${Colors.PRIMARY};
`;

export const Content = styled.View`
  flex-direction: row;
  width: 100%;
  padding: 10px;
  align-items: center;
  justify-content: space-between;
`;

export const ArrowBack = styled(AntDesignIcon).attrs({
  size: 30,
  name: 'arrowleft',
  color: Colors.WHITE,
})``;

export const HomeIcon = styled(MaterialIcons).attrs({
  size: 30,
  color: Colors.WHITE,
  name: 'home',
})``;

export const ExitIcon = styled(MaterialIcons).attrs({
  size: 30,
  color: Colors.WHITE,
  name: 'exit-to-app',
})``;

export const Button = styled.TouchableOpacity.attrs({
  activeOpacity: Metrics.button_opacity,
})``;

// export const HomeIcon = styled(EntypoIcon).attrs({
//   size: 28,
//   color: Colors.WHITE,
//   name: 'home',
// })``;

export const EmptyContainer = styled.View`
  height: 30px;
`;

export const Text = styled.Text.attrs({
  numberOfLines: 1,
})`
  font-size: 30px;
  color: ${Colors.PRIMARY};
  text-align: center;
`;
