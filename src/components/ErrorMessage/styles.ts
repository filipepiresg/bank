import { StyleSheet } from 'react-native';

import { Colors } from '../../styles';

export default StyleSheet.create({
  text: {
    alignSelf: 'flex-end',
    color: Colors.ALERT,
    fontSize: 13,
    textAlign: 'right',
    marginHorizontal: 10,
  },
});
