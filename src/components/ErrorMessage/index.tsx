import React from 'react';
import { Text } from 'react-native';

import Styles from './styles';

interface Props {
  message?: string;
  color?: string;
}

const ErrorMessage: React.FC<Props> = ({ message, color }) => {
  return (
    <Text style={{ ...Styles.text, ...{ color: color || Styles.text.color } }}>{message}</Text>
  );
};

ErrorMessage.defaultProps = {
  message: '',
  color: undefined,
};

export default ErrorMessage;
