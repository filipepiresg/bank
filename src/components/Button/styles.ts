import styled from 'styled-components/native';

import { Colors, Metrics } from '../../styles';

export const Container = styled.TouchableOpacity.attrs({
  activeOpacity: Metrics.button_opacity,
})`
  align-items: center;
  justify-content: center;
  padding: 10px;
  border-radius: 8px;
  background-color: ${Colors.SECONDARY};
  align-self: stretch;
`;

export const ButtonText = styled.Text.attrs({
  numberOfLines: 1,
})`
  text-align: center;
  color: ${Colors.WHITE};
  font-size: 18px;
  font-weight: 600;
`;
