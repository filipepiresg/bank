import React, { memo } from 'react';

import { Container, ButtonText } from './styles';

interface IProps {
  children: string | React.ReactNode;
  onPress: () => void;
  styleButton?: Record<string, unknown>;
}

function Button(props: IProps) {
  const { children, onPress, styleButton = {} } = props;

  return (
    <Container onPress={onPress} style={styleButton}>
      {typeof children === 'string' ? <ButtonText>{children.toUpperCase()}</ButtonText> : children}
    </Container>
  );
}

Button.defaultProps = {
  styleButton: {},
};

export default memo(Button);
