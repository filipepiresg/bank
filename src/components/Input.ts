import styled from 'styled-components/native';

import { Colors } from '../styles';

export default styled.TextInput.attrs({
  placeholderTextColor: Colors.PLACEHOLDER,
})`
  border: 1px solid ${Colors.PLACEHOLDER};
  padding: 10px;
  margin: 10px 0 0;
  font-size: 18px;
  border-radius: 8px;
  background-color: ${Colors.WHITE};
  align-self: stretch;
`;
