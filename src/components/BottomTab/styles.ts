import { StyleSheet } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { opacify } from 'polished';
import styled from 'styled-components/native';

import { Colors } from '../../styles';

export const Container = styled.SafeAreaView`
  background-color: ${Colors.PRIMARY};
  width: 100%;
`;

export const Content = styled.View`
  padding: 10px;
  flex-direction: row;
  margin-bottom: 10px;
  justify-content: space-evenly;
`;

export const TransferIcon = styled(MaterialIcons).attrs({
  size: 35,
  name: 'subdirectory-arrow-right',
})``;

export const ExchangeIcon = styled(MaterialIcons).attrs({
  size: 35,
  name: 'swap-vert',
})``;

export const BalanceeIcon = styled(MaterialIcons).attrs({
  size: 35,
  name: 'account-balance',
})``;

export default StyleSheet.create({
  link: {
    alignItems: 'center',
    justifyContent: 'center',
    // padding: 10,
    alignSelf: 'center',
  },
  underlay: {
    color: opacify(0.2, Colors.PRIMARY),
  },
  activeButton: {
    color: opacify(-0.3, Colors.WHITE),
  },
  desactiveButton: {
    color: Colors.WHITE,
  },
});
