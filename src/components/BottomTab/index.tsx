import React, { memo, useMemo } from 'react';
import { withRouter, RouteComponentProps } from 'react-router';
import { Link } from 'react-router-native';

import Styles, { Container, Content, TransferIcon, ExchangeIcon, BalanceeIcon } from './styles';

const BottomTab: React.FC<RouteComponentProps> = ({ location }) => {
  const path = useMemo<string>(() => location.pathname, [location.pathname]);

  return (
    <Container>
      <Content>
        <Link to='/operations' underlayColor={Styles.underlay.color} style={Styles.link}>
          <TransferIcon
            color={
              path === '/operations' ? Styles.activeButton.color : Styles.desactiveButton.color
            }
          />
        </Link>
        <Link to='/banks' underlayColor={Styles.underlay.color} style={Styles.link}>
          <BalanceeIcon
            color={path === '/banks' ? Styles.activeButton.color : Styles.desactiveButton.color}
          />
        </Link>
        <Link to='/transfers' underlayColor={Styles.underlay.color} style={Styles.link}>
          <ExchangeIcon
            color={path === '/transfers' ? Styles.activeButton.color : Styles.desactiveButton.color}
          />
        </Link>
      </Content>
    </Container>
  );
};

export default memo(withRouter(BottomTab));
