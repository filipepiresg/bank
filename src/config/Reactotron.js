/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-console */
import { NativeModules } from 'react-native';
import Reactotron from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';
import reactotronSaga from 'reactotron-redux-saga';

import AsyncStorage from '@react-native-community/async-storage';

import { Constants } from '../commom';

if (__DEV__) {
  const { scriptURL } = NativeModules.SourceCode;
  const [scriptHostname = ''] = scriptURL.split('://')[1].split(':');

  const tron = Reactotron.configure({
    name: Constants.APP_NAME,
    host: scriptHostname,
  })
    .useReactNative()
    .setAsyncStorageHandler(AsyncStorage)
    .use(reactotronRedux())
    .use(reactotronSaga())

    .connect();

  tron.clear();

  console.tron = tron;
}
