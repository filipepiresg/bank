import axios from 'axios';

export const Endpoints = {
  BASE_URL: 'http://server.5dev.com.br:1337/',
  BANK_ACCOUNT: 'bank-accounts',
  OPERATIONS: 'operations',
  SIGNUP: 'auth/local/register',
  SIGNIN: 'auth/local',
};

export default axios.create({
  baseURL: Endpoints.BASE_URL,
  timeout: 7 * 1000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});
